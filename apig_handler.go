/*
# AWS APIGateway Handler

You will need AWS credentials in a file called "credentials", see $HOME/.aws/credentials and copy/paste.
For example:
```
[default]
aws_access_key_id = blah
aws_secret_access_key = blah blah
```

## To build:
```
GOOS=linux GOARCH=amd64 go apig_handler.go
chmod 644 credentials
zip -j apig_handler.zip apig_handler credentials
```
*/

package main

import (
	"bytes"
	"context"
	"fmt"
	"os"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"

	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
)

// Fake values normally found in events or the environment.
type yfEnvironment struct {
	awsIAMName   string
	s3BucketName string
}

var (
	yfEnv      yfEnvironment
	awsCreds   *credentials.Credentials
	awsConfig  *aws.Config
	awsSession *session.Session
)

func main() {

	yfEnv.awsIAMName = getEnv("AWS_IAM_NAME", "default")
	yfEnv.s3BucketName = getEnv("AWS_S3_BUCKET_NAME", "aws76327.blobs")

	awsCreds = credentials.NewSharedCredentials("credentials", yfEnv.awsIAMName)
	if awsCreds == nil {
		fmt.Printf("FATAL: failed to get credentials\n")
		return
	}

	awsConfig = aws.NewConfig().WithCredentials(awsCreds)
	if awsConfig == nil {
		fmt.Printf("FATAL: failed to get config\n")
		return
	}

	awsSession = session.New(awsConfig)
	if awsSession == nil {
		fmt.Printf("FATAL: failed to get session\n")
		return
	}

	lambda.Start(apigHandler)
}

/*
apigHandler: handle events from the APIGateway
*/
func apigHandler(ctx context.Context, evt events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	var (
		err error
	)

	//	fmt.Printf(">>>apigHandler\n%+v\n", evt)

	err = s3Dump(awsSession, evt.Body)
	if err != nil {
		fmt.Printf("ERROR: s3Dump failed\n")
		return events.APIGatewayProxyResponse{
			Body:       "failed",
			StatusCode: 200,
		}, err
	}

	return events.APIGatewayProxyResponse{
		Body:       "OK",
		StatusCode: 200,
	}, nil
}

/*
s3Dump: dump a string to S3.
The S3 object is named by a timestamp.
*/
func s3Dump(awsSession *session.Session, blob string) error {
	ts := "20060102_150405.000000"
	s3ObjectName := time.Now().Format(ts) + ".json"

	//	fmt.Printf("...dump to object %s\n", s3ObjectName)

	_, err := s3.New(awsSession).PutObject(
		&s3.PutObjectInput{
			Bucket: &yfEnv.s3BucketName,
			Key:    &s3ObjectName,
			Body:   bytes.NewReader([]byte(blob)),
		})

	if err != nil {
		fmt.Printf("ERROR: s3.Put failed: %s\n", err)
	}

	return err
}

func getEnv(key, fallback string) (val string) {
	val = os.Getenv(key)
	if val == "" {
		val = fallback
	}

	return
}
