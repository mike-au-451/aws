# AWS APIGateway Handler

You will need AWS credentials in a file called 'credentials', 
see $HOME/.aws/credentials and copy/paste.
For example:
```
[default]
aws_access_key_id = blah
aws_secret_access_key = blah blah
```

## To build:
```
GOOS=linux GOARCH=amd64 go build apig_handler.go
chmod 644 credentials
zip -j apig_handler.zip apig_handler credentials
```

# AWS Authorization Header

See aws-create-signed.go and aws-example.py

This might be useful for testing the API with curl.  
The original python example comes from AWS documentation, the golang code
is supposed to duplicate it's behaviour, *including* the hard coded features of
the original.


