/*
Create an AWS Authorization header.
This duplicates a python example from AWS docs.
*/

package main

import (
	"crypto/hmac"
	"crypto/sha256"
	"fmt"
	"os"
	"strings"
	"time"
)

type awsCredentials struct {
	awsAccessKeyID     string
	awsSecretAccessKey string
}

type awsConfig struct {
	awsProfile string
	awsRegion  string
}

// Minimum required to produce an Authorization header
type requestValues struct {
	method   string
	service  string
	host     string
	region   string
	endpoint string
}

/*
Usage:
	$0 method service host region endpoint request_parameters

	Empty strings must be present

For Example:
	$0 GET ec2 ec2.amazonaws.com us-east-1 https://ec2.amazonaws.com?Action=DescribeRegions&Version=2013-10-15

	Note that characters like ? and & need to be escaped to prevent the shell from doing the wrong thing
*/
func main() {
	if len(os.Args) != 6 {
		fmt.Printf("FATAL: missing args\n")
		return
	}

	crds := awsCredentials{
		awsAccessKeyID:     getEnv("AWS_ACCESS_KEY_ID", ""),
		awsSecretAccessKey: getEnv("AWS_SECRET_ACCESS_KEY", ""),
	}
	crds = crds //  opinionated language

	cnf := awsConfig{
		awsProfile: "default",
		awsRegion:  "ap-southeast-2",
	}
	cnf = cnf //  opinionated language

	rvs := requestValues{
		method:   strings.ToUpper(os.Args[1]),
		service:  os.Args[2],
		host:     os.Args[3],
		region:   os.Args[4],
		endpoint: os.Args[5],
	}
	rvs = rvs //  opinionated language

	if crds.awsAccessKeyID == "" || crds.awsSecretAccessKey == "" {
		fmt.Printf("FATAL: missing credentials\n")
		return
	}

	// Task 0: collect configuration information
	utcNow := time.Now().UTC()
	// TEST: time stamps need to be consistent to make sure the hashes are identical.
	utcNow = time.Date(2000, time.January, 2, 0, 0, 0, 0, time.UTC)
	// TEST

	dateStamp := utcNow.Format("20060102")
	amzDate := utcNow.Format("20060102T150405Z")
	regionName := rvs.region
	serviceName := rvs.service

	// Task 1: create canonical request
	// Step 1.1: http verb (passed as argument TODO determine what methods are actually supported)
	switch rvs.method {
	case "GET", "POST":
		// do nothing
	default:
		fmt.Printf("FATAL: unsupported method: %s\n", rvs.method)
		return
	}
	// Step 1.2: canonical url (parse URI argument)
	// Step 1.3: canonical query string (parse URI argument)
	canonicalURI, canonicalQuery := rvs.endpoint, ""
	if idx := strings.Index(rvs.endpoint, "?"); idx > 0 {
		canonicalURI, canonicalQuery = rvs.endpoint[:idx], rvs.endpoint[idx+1:]
	}
	//TEST: because Amazon's example hard codes this.
	canonicalURI = "/"
	canonicalQuery = "Action=DescribeRegions&Version=2013-10-15"
	//TEST

	// Step 1.4: canonical headers ()
	canonicalHeaders := "host:" + rvs.host + "\n" + "x-amz-date:" + amzDate + "\n"
	fmt.Printf("...canonicalHeaders: %s\n", canonicalHeaders)

	// Step 1.5: list of signed headers
	signedHeaders := "host;x-amz-date"
	fmt.Printf("...signedHeaders: %s\n", signedHeaders)

	// Step 1.6: payload hash (payload is an empty string for GET requests)
	// Amazon's example hard codes the empty string
	hh := sha256.New()
	hh.Write([]byte("")) // TODO: fix this for POSTs
	payloadHash := fmt.Sprintf("%x", hh.Sum(nil))
	fmt.Printf("...payloadHash: %s\n", payloadHash)

	// Step 1.7: canonical request
	canonicalRequest := rvs.method + "\n" + canonicalURI + "\n" + canonicalQuery + "\n" + canonicalHeaders + "\n" + signedHeaders + "\n" + payloadHash
	fmt.Printf("...canonicalRequest: %s\n", canonicalRequest)

	// Task 2: create the string to sign
	algorithm := "AWS4-HMAC-SHA256" // SHA-1 is also supported
	credentialScope := dateStamp + "/" + rvs.region + "/" + rvs.service + "/" + "aws4_request"
	hh = sha256.New()
	hh.Write([]byte(canonicalRequest))
	stringToSign := algorithm + "\n" + amzDate + "\n" + credentialScope + "\n" + fmt.Sprintf("%x", hh.Sum(nil))
	fmt.Printf("...stringToSign: %s\n", stringToSign)

	// Task 3: create the signature
	// Note that signingKey is binary, not a hex string.
	signingKey := getSignatureKey(crds.awsSecretAccessKey, dateStamp, regionName, serviceName)
	fmt.Printf("...signingKey: %v\n", signingKey)

	hh = hmac.New(sha256.New, signingKey)
	hh.Write([]byte(stringToSign))
	signature := fmt.Sprintf("%x", hh.Sum(nil))
	fmt.Printf("...signature: %s\n", signature)

	// Task 4: Add signature to request
	authorizationHeader := algorithm + " " + "Credential=" + crds.awsAccessKeyID + "/" + credentialScope + ", " + "SignedHeaders=" + signedHeaders + ", " + "Signature=" + signature
	fmt.Printf("...authorizationHeader: %s\n", authorizationHeader)

	headers := fmt.Sprintf("{\"host\": \"%s\", \"x-amz-date\": \"%s\", \"Authorization\": \"%s\"}", rvs.host, amzDate, authorizationHeader)
	fmt.Printf("...headers: %s\n", headers)

	// Task 5: send the request
	requestURL := rvs.endpoint
	if canonicalQuery != "" {
		requestURL = rvs.endpoint + "?" + canonicalQuery
	}

	fmt.Printf("\nBEGIN REQUEST++++++++++++++++++++++++++++++++++++\n")
	fmt.Printf("Request URL = %s\n", requestURL)
	fmt.Printf("headers     =\n%+v\n", headers)
}

func getEnv(key, fallback string) (val string) {
	val = os.Getenv(key)
	if val == "" {
		val = fallback
	}

	return
}

func getSignatureKey(key, dateStamp, regionName, serviceName string) []byte {
	kDate := sign([]byte("AWS4"+key), dateStamp)
	kRegion := sign(kDate, regionName)
	kService := sign(kRegion, serviceName)
	kSigning := sign(kService, "aws4_request")
	return kSigning
}

func sign(key []byte, msg string) []byte {
	hh := hmac.New(sha256.New, key)
	hh.Write([]byte(msg))

	return hh.Sum(nil)
}
